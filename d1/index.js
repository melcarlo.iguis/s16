// Loops

// While Loop

let count = 5;
while( count !== 0){
	console.log('Sylvan');
	count--;
}


let number = 1 ;
while ( number <=5){
	console.log(number);
	number++;
}


let fruits = ['Banana' , 'Mango'];
let index = 0;

while (index <= 1){
	console.log(fruits[index]);
	index++;
}


let mobilePhones = ['Samsung Galaxy' , 'Iphone 13 Pro', 'Xiaomi 11T' , 'Realme C', 'Huawei Nova 8', 'Pixel 5' , 'Asus Rog 6' , 'Cherry Mobile'];
let indexnum = 0 ;

console.log(mobilePhones.length -1);
console.log(mobilePhones[mobilePhones.length -1]);

while (indexnum < mobilePhones.length){
	console.log(mobilePhones[indexnum]);
	indexnum++;
}


// Do- While

let countA = 1;

do {
	console.log("Juan ");
	countA ++ ;
}
while(countA <= 6);


let countB =6; 

do {
	console.log(`Do-while Count ${countB}`);
	countB--;
} 
while(countB == 7);


while(countB == 7 ){
	console.log(`while count ${countB}`);
	countB--;
}


let indexNumA = 0;
let computerBrands = ['Apple Macbook Pro' , 'HP Notebook' , 'Lenovo', 'Acer', 'Dell','Huawei'];

do {
	console.log(computerBrands[indexNumA]);
	indexNumA ++;
}
while(indexNumA <= computerBrands.length -1);


// // For loop

for (let count = 5; count >= 0; count--) {
	console.log(count);
}


let color = ['Red' , 'Green' , 'Blue' , 'Yellow' , 'Purple' , 'White' , 'Black'];


for (let i = 0; i < color.length; i++){
	console.log(color[i]);

}

// Continue & break
// ages
 	// 18, 19, 20 , 21 , 24 , 25


let ages = [18, 19 , 20 , 21 , 24 , 25];

for(i = 0; i < ages.length; i++){
	if(ages[i] == 21 || ages[i] == 18){
		// ages[i] = 27;
		continue;
	}
	console.log(ages[i]);
}






/*
	let studentnames = ['Den' , 'Jayson' , 'Marvin', 'rommel']

*/

let studentNames = ['Den' , 'Jayson' , 'Marvin', 'Rommel'];

for(let i = 0; i < studentNames.length; i++){
	if(studentNames[i] == 'Jayson'){
		console.log(studentNames[i]);
		break;
	}
	// console.log(studentNames[i]);

}



// 1. Given the array 'adultAge', display only adult age range on the console.

// -- the goal of this activity is to exclude values that are not in the range of adult age. Using the tool loops, continue or break, 
// the students must display only the given sample output below on their console.
// */
let adultAge = [20, 23, 33, 27, 18, 19, 70, 15, 55, 63, 85, 12, 19];

for(i = 0; i < adultAge.length; i++){
	if(adultAge[i] <= 19){
		continue;
	}
	console.log(adultAge[i]);
	
}





  
  // -- the goal of this activity is to print only the value needed based on the argument given. Use loop, and continue or break

let students = ['Gary', 'Amelie', 'Anne', 'Jazz', 'Preina', 'James', 'Kelly', 'Diane', 'Lucy', 'Vanessa', 'Kim', 'Francine'];

function searchStudent(studentName){
	for(let i = 0; i < students.length; i++){
		if(students[i]== studentName){
		console.log(students[i]);
		break;
		}
	}
}
searchStudent('Jazz');